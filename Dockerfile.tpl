FROM resin/#{ARCH}-python:#{PYTHON_VERSION}

RUN pip#{PYTHON_VERSION} install numpy==#{NUMPY_VERSION}
