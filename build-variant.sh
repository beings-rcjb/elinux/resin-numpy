#!/bin/bash

die() {
	ra=$1
	shift
	echo "$*" >&2
	exit ${ra}
}

arch=$1 # hardware architecture or board
tag=$2  # Python version

IFS=- read -ra _tagparts <<< ${tag}

python_version=${_tagparts[0]}
numpy_version=${_tagparts[1]}

case "${python_version}" in
	2|3) ;;
	*)
		die 1 "Bad Python version ${python_version}"
esac

sed \
	-e "s@#{ARCH}@${arch}@g" \
	-e "s@#{PYTHON_VERSION}@${python_version}@g" \
	-e "s@#{NUMPY_VERSION}@${numpy_version}@g"\
	Dockerfile.tpl > Dockerfile.${tag}

docker build --pull \
	-f Dockerfile.${tag} -t ${arch}-numpy:${tag} \
	$(dirname $0)

# TODO: Make optional
rm Dockerfile.${tag}
